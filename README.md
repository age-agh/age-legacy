# Legacy AgE versions

This repository contains old AgE versions since 2.2.0 to 2.6.1. Specific
versions are tagged using Git tags.

The documentation for these versions can be found in documentation archives:
https://docs.age.agh.edu.pl/age2/